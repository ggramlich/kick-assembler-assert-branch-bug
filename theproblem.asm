.macro mymacro(value) {
  lda #value
}
.assert "mymacro(0) loads 0", { :mymacro(0) }, {
  lda #0
}
.assert "mymacro(0) loads 0", { :mymacro(0) }, {
  lda #0
}

init:
    /* Under Kick Assembler 5.14 the following line compiles to
    d0 ff
    correct compilation (as it is with Kick Assembler 4.19) is
    d0 03

    Adding more asserts in the above block shifts the negative offset even more.
    */
    bne next

    jmp bye

next:
    nop

bye:
    rts
