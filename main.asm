/*
This demonstrates a bug in the calculation of relative branch addresses
that occurs, when using .assert and later code without setting the
program counter (* directive).

The actual problem occurs in "theproblem.asm"
*/

#import "theproblem.asm"

BasicUpstart2(start)

start:
    jsr init
    jmp start
